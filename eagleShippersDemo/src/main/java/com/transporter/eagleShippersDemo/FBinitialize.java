package com.transporter.eagleShippersDemo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

public class FBinitialize {
	public void initialize() throws IOException {
		try {
		InputStream serviceAccount = this.getClass().getClassLoader().getResourceAsStream("./serviceAccountKey.json");

				FirebaseOptions options = new FirebaseOptions.Builder()
				  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
				  .setDatabaseUrl("https://eagleshippersdemo.firebaseio.com")
				  .setStorageBucket("eagleshippersdemo.appspot.com")
				  .build();
				
			if(FirebaseApp.getApps().isEmpty())
				FirebaseApp.initializeApp(options);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
